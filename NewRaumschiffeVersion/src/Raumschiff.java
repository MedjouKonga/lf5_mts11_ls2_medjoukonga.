import java.util.ArrayList;
import java.util.Random;


/**
 * Klasse Raumschiff.
 */
public class Raumschiff 
{
	
	
	/** The photonentorpedos anzahl. */
	private int photonentorpedosAnzahl;
	
	
	/** The energieversorgung in prozent. */
	private double energieversorgungInProzent;
	
	
	/** The schutzschild in prozent. */
	private double schutzschildInProzent;
	
	
	/** The huelle in prozent. */
	private double huelleInProzent;
	
	
	/** The lebenerhaltungssystem in prozent. */
	private double lebenerhaltungssystemInProzent;
	
	
	/** The name. */
	private String name;
	
	
	/** The reparatur androiden anzahl. */
	private int reparaturAndroidenAnzahl;
	
	
	/** The ladungsverzeichnis. */
	private ArrayList<String> ladungsverzeichnis = new ArrayList<String>();
	
	
	/** The broadcast kommunikator. */
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	
	
	/** The ladung list. */
	private ArrayList<Ladung> ladungList = new ArrayList<Ladung>();
	
	/**
	 * Erstellen ein neues Raumschiff.
	 */
	public Raumschiff()
	{
		
	}
	
	/**
	 * Erstellen ein neues Raumschiff.
	 *
	 * @param photonenA the photonen A
	 * @param energieVIP the energie VIP
	 * @param schutzSIP the schutz SIP
	 * @param huelleIP the huelle IP
	 * @param lebenESIP the leben ESIP
	 * @param name the name
	 * @param reparaturAA the reparatur AA
	 */
	public Raumschiff( int photonenA, double energieVIP, double schutzSIP, double huelleIP, double lebenESIP, String name,  int reparaturAA)
	{
		this.setPhotonentorpedosAnzahl(photonenA);
		this.setEnergieversorgungInProzent(energieVIP);
		this.setSchutzschildInProzent(schutzSIP);
		this.setHuelleInProzent(huelleIP);
		this.setLebenerhaltungssystemInProzent(lebenESIP);
		this.setLebenerhaltungssystemInProzent(lebenESIP);
		this.setName(name);
		this.setReparaturAndroidenAnzahl(reparaturAA);	
	}
	
	/**
	 * Erstellen ein neues Raumschiff.
	 *
	 * @param name the name
	 * @param energieVIP the energie VIP
	 * @param schutzSIP the schutz SIP
	 * @param lebenESIP the leben ESIP
	 * @param huelleIP the huelle IP
	 * @param photonenA the photonen A
	 * @param reparaturAA the reparatur AA
	 * @param ladungV the ladung V
	 */
	public Raumschiff(String name, double energieVIP, double schutzSIP, double lebenESIP, double huelleIP, int photonenA, int reparaturAA, ArrayList<String> ladungV)
	{
		this.setName(name);
		this.setEnergieversorgungInProzent(energieVIP);
		this.setSchutzschildInProzent(schutzSIP);
		this.setLebenerhaltungssystemInProzent(lebenESIP);
		this.setHuelleInProzent(huelleIP);
		this.setPhotonentorpedosAnzahl(photonenA);
		this.setReparaturAndroidenAnzahl(reparaturAA);
		this.setLadungsverzeichnis(ladungV);
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	
	/**
	 * Gets the energieversorgung in prozent.
	 *
	 * @return the energieversorgung in prozent
	 */
	public double getEnergieversorgungInProzent()
	{
		return this.energieversorgungInProzent;
	}
	
	/**
	 * Sets the energieversorgung in prozent.
	 *
	 * @param energieVIP the new energieversorgung in prozent
	 */
	public void setEnergieversorgungInProzent(double energieVIP)
	{
		this.energieversorgungInProzent = energieVIP;
	}
	
	/**
	 * Gets the schutzschild in prozent.
	 *
	 * @return the schutzschild in prozent
	 */
	public double getSchutzschildInProzent()
	{
		return this.schutzschildInProzent;
	}
	
	/**
	 * Sets the schutzschild in prozent.
	 *
	 * @param schutzSIP the new schutzschild in prozent
	 */
	public void setSchutzschildInProzent(double schutzSIP)
	{
		this.schutzschildInProzent = schutzSIP;
	}
	
	/**
	 * Gets the lebenerhaltungssystem in prozent.
	 *
	 * @return the lebenerhaltungssystem in prozent
	 */
	public double getLebenerhaltungssystemInProzent()
	{
		return this.lebenerhaltungssystemInProzent;
	}
	
	/**
	 * Sets the lebenerhaltungssystem in prozent.
	 *
	 * @param lebenESIP the new lebenerhaltungssystem in prozent
	 */
	public void setLebenerhaltungssystemInProzent(double lebenESIP)
	{
		this.lebenerhaltungssystemInProzent = lebenESIP;
	}
	
	/**
	 * Gets the huelle in prozent.
	 *
	 * @return the huelle in prozent
	 */
	public double getHuelleInProzent()
	{
		return this.huelleInProzent;
	}
	
	/**
	 * Sets the huelle in prozent.
	 *
	 * @param huelleIP the new huelle in prozent
	 */
	public void setHuelleInProzent(double huelleIP)
	{
		this.huelleInProzent = huelleIP;
	}
	
	/**
	 * Gets the photonentorpedos anzahl.
	 *
	 * @return the photonentorpedos anzahl
	 */
	public int getPhotonentorpedosAnzahl()
	{
		return this.photonentorpedosAnzahl;
	}
	
	/**
	 * Sets the photonentorpedos anzahl.
	 *
	 * @param photonAnzahl the new photonentorpedos anzahl
	 */
	public void setPhotonentorpedosAnzahl(int photonAnzahl)
	{
		this.photonentorpedosAnzahl = photonAnzahl;
	}
	
	/**
	 * Gets the reparatur androiden anzahl.
	 *
	 * @return the reparatur androiden anzahl
	 */
	public int getReparaturAndroidenAnzahl()
	{
		return this.reparaturAndroidenAnzahl;
	}
	
	/**
	 * Sets the reparatur androiden anzahl.
	 *
	 * @param reparaturAA the new reparatur androiden anzahl
	 */
	public void setReparaturAndroidenAnzahl(int reparaturAA )
	{
		this.reparaturAndroidenAnzahl = reparaturAA;
	}
	
	/**
	 * Gets the ladungsverzeichnis.
	 *
	 * @return the ladungsverzeichnis
	 */
	public ArrayList<String> getLadungsverzeichnis()
	{
		return this.ladungsverzeichnis;
	}
	
	/**
	 * Sets the ladungsverzeichnis.
	 *
	 * @param ladungV the new ladungsverzeichnis
	 */
	public void setLadungsverzeichnis(ArrayList<String> ladungV )
	{
		this.ladungsverzeichnis = ladungV;
	}
	
	/**
	 * Gets the broadcast kommunikator.
	 *
	 * @return the broadcast kommunikator
	 */
	public ArrayList<String> getBroadcastKommunikator()
	{
		return this.broadcastKommunikator;
	}
	
	/**
	 * Sets the broadcast kommunikator.
	 *
	 * @param broadcastK the new broadcast kommunikator
	 */
	public void setBroadcastKommunikator(ArrayList<String> broadcastK)
	{
		this.broadcastKommunikator = broadcastK;
	}
	
	/**Vorausstezung.: Falls das als Parameter uebergebene Raumschiff existiert,
	 *(Effekt.:)gibt es keine Torpedos, wird eine Nachricht an Alle ausgegeben;
	 *gibt es Torpedos, wird die Torpedoanzahl um eins reduziert; eine Nachricht an Alle gesendet.
	 *Andernfalls geschieht nichts.
	 * @param raumschiff das abzuschiessende Raumschiff.
	 */
	public void photonentorpedosAbschliessen(Raumschiff raumschiff)
	{
		if(raumschiff != null)
		{
			if(this.photonentorpedosAnzahl == 0)
			{
				nachrichtenSenden("-=*Click*=-");// geht nur, wenn es keine Torpedos gibt.
			}
			else
			{
				this.photonentorpedosAnzahl--;
				nachrichtenSenden("Photonentorpedo abgeschossen");
				trefferVermeken(raumschiff);
			}
			
		}
			
		
	}
	
	/**Voraussetzung.: Falls das als Parameter uebergebene Raumschiff existiert,
	 *(Effekt.:) ist die Energieversorgung kleiner als 50%, wird eine Nachricht Alle ausgeben;
	 *ist die Energieversorgung groesser als 50%, wird die Energieversorgung um 50% reduziert, eine Nachricht an Alle gesendet.
	 *Andernfalls geschieht nichts.
	 * @param raumschiff das abzuschiessende Raumschiff.
	 */
	public void phaserKanonenAbschliessen(Raumschiff raumschiff)
	{
		if(raumschiff != null)
		{
			if(this.energieversorgungInProzent < 50)
			{
				nachrichtenSenden("-=*Click*=-");
			}
			else
			{
				this.energieversorgungInProzent -= 0.5 * this.energieversorgungInProzent ;
				nachrichtenSenden("Phaserkanone abgeschossen");
				trefferVermeken(raumschiff);
			}
		}
		
	}
	
	/**Vorausstezung.: Falls das als Parameter uebergebene Raumschiff existiert:
	 * (Effekt.:) wird eine Nachricht in der Konsole ausgegeben; wird der Wert 
	 * der Schilde des als Parameter uebergebenen Schiffes um 50% reduziert; sollte
	 * die Schilde vollstaendig zerstoert worden sein, wird die huelle und die Energieversorgung
	 * um 50% abgebaut; sollte die huelle auf 0% absinken, sind die Lebenserhaltungssysteme vollstaendig
	 * zerstoert und es wird eine Nachricht an Alle ausgegeben.
	 * Andernfalls geschieht nichts.
	 * @param raumchiff das getroffene Raumschiff
	 */
	public void trefferVermeken(Raumschiff raumchiff)
	{
		if(raumchiff != null)
		{
			raumchiff.schutzschildInProzent-= 0.5 * raumchiff.schutzschildInProzent;
			if(raumchiff.schutzschildInProzent == 0)
			{
				raumchiff.huelleInProzent-= 0.5 * raumchiff.huelleInProzent;
				raumchiff.energieversorgungInProzent-= 0.5 * raumchiff.energieversorgungInProzent;
			}
			if(raumchiff.huelleInProzent == 0)
			{
				nachrichtenSenden("Die Lebenerhaltungssysteme sind vernichtet worden.");
			}
			System.out.println(raumchiff.name +" " + "wurde getroffen");
			
		}
	}
	
	/**Nachrichten senden.
	 * @param nachricht die Nachricht in der Konsole auszugeben 
	 * und im BroadcastKommunikator hinzuzufuegen.
	 */
	public void nachrichtenSenden(String nachricht)
	{
		System.out.println(nachricht);
		broadcastKommunikator.add(nachricht);
	}
	
	/**Voraussetzung.: Falls die als Parameter uebergebene Ladung existiert,
	 * (Effekt.:)wird sie in der Ladungslist hinzugefuegt.
	 * Andernfalls geschieht nichts. 
	 * @param ladung die zu ladende Ladung.
	 */
	public void raumschiffBeladen(Ladung ladung)
	{
		if(ladung != null)
		{
			ladungList.add(ladung);
		}
		
	}
	
	/** Gibt den broadcastKommunikator zurueck.
	 * @return Arraylist
	 */
	public static ArrayList<String> alleLogbuecherGeben()
	{
		ArrayList<String> eintraegeDesLogbuchs = broadcastKommunikator;
		return eintraegeDesLogbuchs ;
	}
	/**
	 * Gibt es keine Ladung auf dem Schiff, wird zei verschiedenen Nachrichten in der Konsole ausgegeben.
	 * Ist die Anzahl die als Parameter uebergebene torpedoAnzahl groesser als die Menge der
	 * tatsaechlich Vorhandenen, werden alle vorhandenen Photonentorpedos eingesetzt.
	 * Ansonsten wird die torpedoAnzahl vermindert und die Anzahl der eingesetzten Torpedos
	 * wird auf  der Konsole ausgegeben.
	 *
	 * @param torpedoAnzahl the torpedo anzahl
	 */
	public void photonentorpedosEinsetzen(int torpedoAnzahl)
	{
		if(this.photonentorpedosAnzahl == 0)
		{
			System.out.println("Keine Photonentorpedos gefunden");
			nachrichtenSenden("=*Clicck*=");
		}
		if(torpedoAnzahl >= this.photonentorpedosAnzahl)
		{
			System.out.println("Alle vorhandenen Photonentorpedos werden eingesetzt.");
			this.photonentorpedosAnzahl = 0;
		}
		else 
		{
			this.photonentorpedosAnzahl-= torpedoAnzahl;
			System.out.println(torpedoAnzahl + "Photonentorpedos werden eingesetzt.");
		}
	}
	
	/**
	 * Reparatur auftrag senden.
	 *
	 * @param istSchutzschilde the ist schutzschilde
	 * @param istEnergieVersorgung the ist energie versorgung
	 * @param istSchiffhuelle the ist schiffhuelle
	 * @param anzahlEingesetztenAndroiden the anzahl eingesetzten androiden
	 */
	public void reparaturAuftragSenden(boolean istSchutzschilde, boolean istEnergieVersorgung, boolean istSchiffhuelle, int anzahlEingesetztenAndroiden)
	{ 
		double trueAnzahl = 0;
		if(istSchutzschilde)
		{
			System.out.println("Schutzschilde repariert werden sollen");
			trueAnzahl ++;
			
		}
		if(istEnergieVersorgung)
		{
			System.out.println("Energieversorgung repariert werden sollen");
			trueAnzahl ++;
			
		}
		if(istSchiffhuelle)
		{
			System.out.println("Schiffhuelle repariert werden sollen");
			trueAnzahl ++;
			
		}
		Random random =new Random();
		int zufallszahl = random.nextInt(100);
		double prozentualeRepariertenSchiffsstrukturen = (zufallszahl * anzahlEingesetztenAndroiden)/ trueAnzahl;
		
		if(istSchutzschilde)
		{
			System.out.printf("prozentuale der reparierten Schiffstrukture: %f", prozentualeRepariertenSchiffsstrukturen);
			
		}
		if(istEnergieVersorgung)
		{
			System.out.printf("prozentuale der reparierten Schiffstrukture: %f", prozentualeRepariertenSchiffsstrukturen);
			
		}
		if(istSchiffhuelle)
		{
			System.out.printf("prozentuale der reparierten Schiffstrukture: %f", prozentualeRepariertenSchiffsstrukturen);
			
		}	
	}

    /**
     * Adds the ladung.
     *
     * @param neueLadung the neue ladung
     */
    public void addLadung(Ladung neueLadung)
    {
    	if(neueLadung != null)
    	{
    		ladungList.add(neueLadung);
    	}
    	
    }
    
    /**
     * Ladung verzeichnis ausgeben.
     */
    public void ladungVerzeichnisAusgeben()
    {
    	System.out.println("-------Ladungsverzeichnis von " + name + "------------------" );
    	for (int i = 0; i < ladungList.size(); i++)
    	{ if(ladungList.get(i).getAnzahl() != 0)
    	    {
    		System.out.printf("%-10s:%10d%n", ladungList.get(i).getName(), ladungList.get(i).getAnzahl());
    	    }
    	}
    }
    
    /**
     * Zustand raumschiff ausgeben.
     */
    public void zustandRaumschiffAusgeben()
	    {
	    	System.out.println("--------" + "Raumschiffzustand von " + name + "------------");
	    	System.out.printf("photonentorpedosAnzahl: %d%n",photonentorpedosAnzahl );
	    	System.out.printf("energieversorgungInProzent: %.2f%n", energieversorgungInProzent);
	    	System.out.printf("schutzschildInProzent: %.2f%n", schutzschildInProzent);
	    	System.out.printf("huelleInProzent: %.2f%n",huelleInProzent);
	    	System.out.printf("lebenerhaltungssystemInProzent: %.2f%n",lebenerhaltungssystemInProzent);
	    	System.out.printf("name: %s%n",name);
	    	System.out.printf("reparaturAndroidenAnzahl: %d%n",reparaturAndroidenAnzahl);
	    }

}
