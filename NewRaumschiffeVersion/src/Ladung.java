/**
 * The Class Ladung.
 */
public class Ladung 
{
	
	private String name;
	
	
	private int anzahl;
	
	
	public Ladung()
	{
		
	}
	
	/**
	 * Erstellen eine neue ladung.
	 *
	 * @param name 
	 * @param anzahl 
	 */
	public Ladung(String name, int anzahl)
	{
		this.setName(name);
		this.setAnzahl(anzahl);
	}
	
	
	public String getName()
	{
		return this.name;
	}
	
	 
	public void setName(String name)
	{
		this.name = name;
	}
	
	
	public int getAnzahl()
	{
		return this.anzahl;
	}
	
	
	public void setAnzahl(int anzahl)
	{
		this.anzahl =anzahl; 
	}

}
