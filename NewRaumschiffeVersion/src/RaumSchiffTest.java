
public class RaumSchiffTest 
{

	public static void main(String[] args) 
	{
		Ladung ladung1 = new Ladung("Ferengi Schneknsat", 200);
		Ladung ladung2 = new Ladung("Borg-Schrott", 5);
		Ladung ladung3 = new Ladung("Rote Materie", 2);
		Ladung ladung4 = new Ladung("Forschungssonde", 35);
		Ladung ladung5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung ladung6 = new Ladung("Plasma-Waffe", 50);
		Ladung ladung7 = new Ladung("Photonentorpedo", 3);
		
		Raumschiff klingonen = new Raumschiff(0, 100, 100, 100, 100, "IKS Hegh'ta", 2);
		Raumschiff romulaner = new Raumschiff(2, 50, 100, 100, 100, "IRW Khazara", 2);
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, "Ni'Var", 5);
		
		klingonen.raumschiffBeladen(ladung1);
		klingonen.raumschiffBeladen(ladung5);
		
		romulaner.raumschiffBeladen(ladung2);
		romulaner.raumschiffBeladen(ladung3);
		romulaner.raumschiffBeladen(ladung6);
		
		vulkanier.raumschiffBeladen(ladung4);
		vulkanier.raumschiffBeladen(ladung7);
		
		klingonen.photonentorpedosAbschliessen(romulaner);
		romulaner.phaserKanonenAbschliessen(klingonen);
		vulkanier.nachrichtenSenden("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiffAusgeben();
		klingonen.ladungVerzeichnisAusgeben();
		klingonen.photonentorpedosAbschliessen(romulaner);
		klingonen.photonentorpedosAbschliessen(romulaner);
		klingonen.zustandRaumschiffAusgeben();
		klingonen.ladungVerzeichnisAusgeben();
	    romulaner.zustandRaumschiffAusgeben();
		romulaner.ladungVerzeichnisAusgeben();
		vulkanier.zustandRaumschiffAusgeben();
		vulkanier.ladungVerzeichnisAusgeben();

	}

}
