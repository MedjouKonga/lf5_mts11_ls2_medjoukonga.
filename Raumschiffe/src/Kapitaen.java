/**
 * Klasse Kapitaen.
 */
public class Kapitaen 
{
	
	
	private String geschlecht;
	
	
	private String name;
	
	
	private int jahrMachtuebernahme;
	
	
	public Kapitaen()
	{
		
	}
	
	/**
	 * Erstellen eine neue kapitaen.
	 *
	 * @param geschlecht the geschlecht
	 * @param name the name
	 * @param jahrMachtuebernahme the jahr machtuebernahme
	 */
	public Kapitaen(String geschlecht, String name, int jahrMachtuebernahme)
	{
		this.geschlecht = geschlecht;
		this.name = name;
		this.jahrMachtuebernahme = jahrMachtuebernahme;
		
	}
	
	public String getGeschlecht()
	{
		return this.geschlecht;
	}
	
	public void setGeschlecht(String geschlecht )
	{
		this.geschlecht = geschlecht;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String name )
	{
		this.name = name;
	}
	
	public int getJahrMachtuebernahme()
	{
		return this.jahrMachtuebernahme;
	}
	
	public void setJahrMachtuebernahme(int jahrMachtuebernahme )
	{
		this.jahrMachtuebernahme = jahrMachtuebernahme;
	}

}
