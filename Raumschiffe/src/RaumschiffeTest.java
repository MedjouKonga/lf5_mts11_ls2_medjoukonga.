import java.util.ArrayList;
import java.util.Random;

public class RaumschiffeTest 
{

	public static void main(String[] args) 
	{
		
		Ladung ladung1 = new Ladung("Ferengi Schneknsat", 200);
		Ladung ladung2 = new Ladung("Borg-Schrott", 5);
		Ladung ladung3 = new Ladung("Rote Materie", 2);
		Ladung ladung4 = new Ladung("Forschungssonde", 35);
		Ladung ladung5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung ladung6 = new Ladung("Plasma-Waffe", 50);
		Random random = new Random();
		int zufallszahl = random.nextInt(100);
		
		Raumschiff klingonen = new Raumschiff(0, 100, 50, 100, 100, "IKS Hegh'ta", 2);
		Raumschiff romulaner = new Raumschiff(2, 50, 50, 100, 100, "IRW Khazara", 2);
		Raumschiff vulkanier = new Raumschiff(3, zufallszahl, zufallszahl,zufallszahl, 100, "Ni'Var", 5);
		
		klingonen.raumschiffBeladen(ladung1);
		klingonen.raumschiffBeladen(ladung5);
		
		romulaner.raumschiffBeladen(ladung2);
		romulaner.raumschiffBeladen(ladung3);
		romulaner.raumschiffBeladen(ladung6);
		
		vulkanier.raumschiffBeladen(ladung4);
		
		
		klingonen.photonentorpedosAbschliessen(romulaner);
		romulaner.phaserKanonenAbschliessen(klingonen);
		vulkanier.nachrichtenSenden("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiffAusgeben();
		klingonen.ladungVerzeichnisAusgeben();
		vulkanier.photonentorpedosEinsetzen(vulkanier.getPhotonentorpedosAnzahl());
		vulkanier.photonentorpedosInTorpedorohreLaden(vulkanier.getPhotonentorpedosAnzahl());
		klingonen.photonentorpedosAbschliessen(romulaner);
		klingonen.photonentorpedosAbschliessen(romulaner);
		klingonen.zustandRaumschiffAusgeben();
		klingonen.ladungVerzeichnisAusgeben();
	    romulaner.zustandRaumschiffAusgeben();
		romulaner.ladungVerzeichnisAusgeben();
		vulkanier.zustandRaumschiffAusgeben();
		vulkanier.ladungVerzeichnisAusgeben();
		
		ArrayList<String> broadcastKommunikator = Raumschiff.alleLogbuecherGeben();
		
		System.out.println("-----------------BroadcastKommunikator--------------------------------------");
		
		for(int i = 0; i < broadcastKommunikator.size(); i++)
		{
			System.out.println(broadcastKommunikator.get(i));
		}
		
	
	}

}
