
public class Ladung 
{
	private String name;
	private int anzahl;
	
	
	public Ladung()
	{
		
	}
	public Ladung(String name, int anzahl)
	{
		this.name = name;
		this.anzahl = anzahl;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public int getAnzahl()
	{
		return this.anzahl;
	}
	
	public void setAnzahl(int anzahl)
	{
		this.anzahl =anzahl; 
	}
	/**
	 * @return: gibt den Name und die Anzahl die Ladung zurueck.
	 */
	
	public @Override String toString() 
	{
		return this.name + " " + this.anzahl ;
	}
}
